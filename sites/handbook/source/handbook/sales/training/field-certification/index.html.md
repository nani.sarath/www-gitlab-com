---
layout: handbook-page-toc
title: "Field Certification Program"
description: "To support and scale GitLab’s continued growth and success, the Field Enablement Team is developing a role based certification program that includes functional, soft skills, and technical training for field team members"
---

## Field Certification Program 
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Field Certification Program Overview 
To improve sales efficiency & predictability with a consistent and scalable customer engagement, the Field Enablement Team is developing a role based certification program that includes functional, soft skills, and [product training](/handbook/sales/training/product-certification/) for field team members.  

The Certifications will align to the customer journey and the [field functional competencies](/handbook/sales/training/field-functional-competencies/) that address the critical knowledge, skills, role-based behaviors, processes, content, and tools to successfully execute customer engagements.

Note: This certification program is different from resources like other GitLab certifications that focus on how to _use GitLab_ or tied to GitLab competencies (though we encourage field team members to take these as well)! 
- [GitLab Certifications](/handbook/people-group/learning-and-development/certifications/)
- [GitLab Technical Certifications](/handbook/customer-success/professional-services-engineering/gitlab-technical-certifications/)

### Purpose of the Field Certification Program
Certification programs allow us to validate GitLab skills and knowledge, recognize individual mastery, and motivate continuous learning and skills growth all aligned to expectation setting and performance discussions.

The goal of the Field Certification Program is to validate that:
* Knowledge has been transferred (Do you know it?)
* The learner can apply that knowledge in simulated & live scenarios (Can you do it?)
* The learner has taken ownership of effective skill execution and can demonstrate proficiency over time (Can you do it independently and continue to execute over time?)

### Audience 
The Field Certification Program will include functional and soft skills training for:
* [Strategic Account Leaders](/handbook/sales/training/field-certification/sal/)
* [Account Executives](/handbook/sales/commercial/enablement/required7/)  
* [Solution Architects](/handbook/sales/training/field-certification/sa/)  
* [Technical Account Managers](/handbook/sales/training/field-certification/tam/)
* [Partners](/handbook/resellers/training/)

## Prerequisites
During [Sales Quick Start](/handbook/sales/onboarding/), every GitLab field team member is trained on [customer value drivers](/handbook/sales/command-of-the-message/#customer-value-drivers) and GitLab’s value-based messaging framework (see the [Command of the Message](/handbook/sales/command-of-the-message/) page). In addition, GitLab field team members and partners are introduced to GitLab’s [customer use cases](/handbook/use-cases/).

### Architecture and Approach 
With the customer in mind, learning will be focused on helping field team member create engagements with a customer that make a lasting emotional impression. The foundation of this approach is based on the concept explained in "Blueprints for a SAAS Sales Organization: How to Design, Build and Scale a Customer Centric Sales Orgnization" by Jacob Van Der Kooj and Fernando Pizarro. 

Please note: Processes continue to mature and evolve. To see that latest, please check out the slide below. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQNYu_4jB3j3i_fYukM3yMtcbhgbpKbivbaaiKnuih3X6pdn-oI9ic0k6TqtwP2qjqVAaC-HXIC0uD7/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Delivery 
Course content will be a combination of handbook pages, YouTube videos, and interactive content delivered through the new Learning Experience Platform (LXP). This new system is not intended to replace the Handbook as the single source of truth, and instead will serve to curate content from a variety of sources and provide a guided tour of curated content for the learner. 

### Certification Assessments
To achieve GitLab “certified” status, candidates must complete both the online written exam (knowledge) and practical assessment (skill) with a passing score on each exam.
* Anyone can sign up and take the written exam, but only learners who successfully pass the online written assessment will be granted access to the practical assessment
* The practical assessment will require team members to demonstrate the ability to deliver customer engagements to GitLab’s standards 
* Upon completion of all courses within each certification, learners will receive a badge

![field-cert-badges](/handbook/sales/training/field-certification/field-cert-badges.png)

## Roadmap
Alpha testing for the below courses will take place in FY22Q2:
- Account Planning for Strategic Account Leaders
- The Required 7 for Account Executives: Practical Assessment  
- Demo Skills with Demo2Win 
- Executive Business Reviews for Technical Account Managers 

This is a roadmap of what to expect in the next quarter, but do not wait to be assigned courses. You can search and consume courses on your own schedule by searching in [GitLab Learn](https://gitlab.edcast.com/) our learning experience platform.  

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQvjB6E9JlplzwqBHVv2fFGAEGZwqjg4AZQO-p_DqjX7znjZGOC_q2-d2xCbwr2LbfXCmyOvVxcirYb/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

### Customer Use Case Training

Additional deeper dive customer use case training focused on **how to sell GitLab** will be made available to field team members and partners and include the below elements:
- Market Overview
- Target Buyer and User Personas
- Industry Insights
- Market Requirements
- How GitLab Does It
- How GitLab Does it Better
- Getting the Meeting
- Proof Points
- Competitive Comparisons
- Application Exercise

Currently available courses include the following:

#### Continuous Integration (CI)

- Access the [Continuous Integration Customer Use Case learning path](https://gitlab.edcast.com/insights/ECL-ac43cbf8-722b-4b0f-9b9c-e5132aa6d7b0)  

#### DevSecOps

- Access the [DevSecOps Customer Use Case learning path](https://gitlab.edcast.com/pathways/ECL-ec3486b7-6b30-4a00-b638-4d482fb236c8)

#### DevOps Platform 

- Access the [DevOps Platform Use Case learning path](https://gitlab.edcast.com/insights/ECL-b6672a5d-a33e-4c0e-850e-e72c5f8be001)


## Feedback 

To get involved or follow along with the development, check out the [Field Certification Issues Board](https://gitlab.com/groups/gitlab-com/sales-team/-/boards/1637426). We love feedback and questions, so check us out on Slack in #field-cert-program. 

