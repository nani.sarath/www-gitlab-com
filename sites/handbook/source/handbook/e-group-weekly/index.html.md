---
layout: handbook-page-toc
title: "E-Group Weekly"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Purpose

For [executives](/company/team/structure/#executives) to connect on a weekly basis in order to:

1. Get timely input from E-Group
1. Align on key initiatives
1. Inform about key business happenings 
1. Celebrate company successes

## Timing
Scheduled for 80 minutes on Mondays. These don't occur during 
[E-Group Offsite](/company/offsite/)
weeks and may occasionally be rescheduled for another day due to calendar conflicts or holidays. 

## Attendees
1. [Executives](/company/team/structure/#executives)
1. [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/); when not possible, the [Director, Strategy and Operations](/job-families/chief-executive-officer/strategy-and-operations/)
1. [CEO Shadows](/handbook/ceo/shadow/)
1. [Executive Business Admin](/eba/#executive-business-administrator-team) to the CEO (optional)
1. Invited participants: folks invited to participate in one or more specific session

## Scheduling
The [EBA to the CEO](https://about.gitlab.com/handbook/eba/#executive-business-administrator-team) is responsible for scheduling these meetings. [Chief of Staff](https://about.gitlab.com/job-families/chief-executive-officer/chief-of-staff/#directed-work) is responsible for managing to the agenda and ensuring that meetings don't exceed the allocated time. 

## Meeting Details
1. All agenda items including any pre-read material should be added to the agenda by 5pm PT on the business day immediately before the meeting unless something unexpected happens between the end of normal submissions and when the meeting occurs
1. If pre-work is strongly encouraged, it should be flagged as early possible in the week before in #e-group or another appropriate Slack channel
1. Each agenda item should clearly state the objective for the topic. For example:
   1. Inform about upcoming announcement
   1. Get feedback on a proposal
   1. Make a decision on the Contribute location
1. If you are inviting external folks to a specific session, please notify the EBA to the CEO at least two business days before the meeting
1. Ensure that any invited participants are clear about their role in the discussion

